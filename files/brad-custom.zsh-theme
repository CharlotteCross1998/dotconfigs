if [[ $UID == 0 || $EUID == 0 ]]; then
	PROMPT='%F{208}♛ %F{red}~/ %f'
	RPROMPT="%F{163}[%F{160}%*%F{163}]"
else
	PROMPT='%F{208}λ %F{blue}~/ %f'
	RPROMPT="%F{163}[%F{160}%*%F{163}]"
fi
