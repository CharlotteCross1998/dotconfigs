#
# ~/.bashrc
#

xset -b #stop that annoying beeping

[[ $- != *i* ]] && return

colors() {
	local fgc bgc vals seq0

	printf "Color escapes are %s\n" '\e[${value};...;${value}m'
	printf "Values 30..37 are \e[33mforeground colors\e[m\n"
	printf "Values 40..47 are \e[43mbackground colors\e[m\n"
	printf "Value  1 gives a  \e[1mbold-faced look\e[m\n\n"

	# foreground colors
	for fgc in {30..37}; do
		# background colors
		for bgc in {40..47}; do
			fgc=${fgc#37} # white
			bgc=${bgc#40} # black

			vals="${fgc:+$fgc;}${bgc}"
			vals=${vals%%;}

			seq0="${vals:+\e[${vals}m}"
			printf "  %-9s" "${seq0:-(default)}"
			printf " ${seq0}TEXT\e[m"
			printf " \e[${vals:+${vals+$vals;}}1mBOLD\e[m"
		done
		echo; echo
	done
}

[[ -f ~/.extend.bashrc ]] && . ~/.extend.bashrc

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion


set completion-ignore-case on
set completion-map-case on

#aliases
alias exit~=exit
alias pping="ping 192.168.0.1" #because my network card often fails randomly
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias cmesg="dmesg -T|sed -e 's|\(^.*'`date +%Y`']\)\(.*\)|\x1b[0;33m\1\x1b[0m - \2|g'" #dmesg with coloured output

# append to the history file, don't overwrite it
shopt -s histappend

#prompt | (orange)λ (blue)~/ (pink)<text>
export PS1="\033[38;5;208mλ \[\e[34m\]~/\[\e[m\]\033[38;5;199m "

#Man Page Colours
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;33m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;32m'

#Very important
echo "uwu"

export PATH="/usr/games:$PATH"
force_color_prompt=yes

#also important
function uwu() { echo uwu; }
