# dotconfigs
Where all my config files go
(All screenshots are in the screenshot folder)

# Features
## Bash Config
* Coloured Man Pages
* Custom Lambda Theme

Screenshot:

<img src="http://i.imgur.com/4VDJVIN.png"></img>

## ZSH Theme
Custom Lambda Theme (When Not Root)

Custom King Theme (When Root)

Screenshot:

<img src="https://i.imgur.com/cMhJuPp.png"></img>

Root:

<img src="https://i.imgur.com/BBCmTeB.png"></img>

## ZSH Config
* Coloured Man Pages

## Neofetch Config

<img src="http://i.imgur.com/9ckF1Ea.png"></img>

# urxvt Config

* Transparency
* Borderless Window
* Nice Colours


<img src="http://i.imgur.com/YpsKDpx.png"></img>
